﻿using E_FInventario.ModelosAuxiliar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace E_FInventario
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //obtenerTodosLosProducto();
            //poblarDataGridProducto();
            //poblarDataGripPersonalizado();

            this.DataContext = new producto();

        }





        public void poblarDataGripPersonalizado()
        {

            InventarioDosEntities db = new InventarioDosEntities();
            var listaProductos = from p in db.productoes
                                 select new ProductoAxiliar
                                 {
                                     ID = p.id,
                                     CODIGO = p.codigo ,
                                     NOMBRE = p.nombre,
                                     PRECIO = p.precio == null ? 0 : (decimal)p.precio
                                 };

            this.gridProductosPersonalizado.ItemsSource = listaProductos.ToList();

        }

        public void mostrarTablaPersonalizada(object sender, RoutedEventArgs e)
        {
            poblarDataGripPersonalizado();
        }


        /*
        if(p.precio == null) {
            p.precio = 0
        }else {
         (decimal)p.precio
        }

         
         */



        public void poblarDataGridProducto()
        {

            InventarioDosEntities db = new InventarioDosEntities();
            var listaProductos = from p in db.productoes
                                 select p;

            this.gridProductos.ItemsSource = listaProductos.ToList();

        }

        private void mostrarGrid(object sender, RoutedEventArgs e)
        {
            poblarDataGridProducto();

        }        





            public void obtenerTodosLosProducto()
        {
            InventarioDosEntities db = new InventarioDosEntities();

            // EF
            // select * from producto
            // from 
            // LINQ -->
            // from cualquiernmbre in db.productoes select cualquiernmbre; 
            var productos = from p in db.productoes
                            select p;

            // C#
            foreach (var prod in productos)
            {
                Console.WriteLine(prod.id + " - "+ prod.codigo + " - " + prod.nombre + " - " + prod.precio);
            }


            /*
        public Producto consultar(SqlConnection conn, String instruccion)
         {
            Producto producto = new Producto();
            // Abrir conn
            conn.Open();
            SqlCommand comando = new SqlCommand(instruccion, conn);
            SqlDataReader registros = comando.ExecuteReader();
            while (registros.Read())
            {
                Console.WriteLine("Select  Correcta");
                Console.WriteLine(registros["id"].ToString());
                producto.Id = Convert.ToInt32(registros["id"]);
            }  
            //Cerrar
            conn.Close();
            return producto;
        }
             */
        }

        private void guardarRegistro(object sender, RoutedEventArgs e)
        {
            producto pDataContext = (producto)this.DataContext;
            InventarioDosEntities db = new InventarioDosEntities();
            if(pDataContext.id == 0)
            {

                producto productoAguardar = new producto()
                {
                    codigo = pDataContext.codigo,
                    nombre = pDataContext.nombre,
                    precio = pDataContext.precio

                };
                db.productoes.Add(productoAguardar);
                db.SaveChanges();
                // Ingresar un nuevo
                this.DataContext = new producto();
                MessageBox.Show("Producto: " + productoAguardar.nombre + " guardado!!!!");
            }else
            {

                var productoBaseDatos = from p in db.productoes
                                        where p.id == pDataContext.id
                                        select p;
                producto prod = productoBaseDatos.SingleOrDefault();

                if(prod != null)
                {
                    prod.nombre = pDataContext.nombre;
                    prod.codigo = pDataContext.codigo;
                    prod.precio = pDataContext.precio;
                }

                db.SaveChanges();
                this.DataContext = new producto();
                MessageBox.Show("Producto: " + pDataContext.nombre + " Actualizado!!!!");
            }


        }



        private void eliminarProducto(object sender, RoutedEventArgs e)
        {
            InventarioDosEntities db = new InventarioDosEntities();
            producto pDataContext = (producto)this.DataContext;

            var prod = from p in db.productoes
                       where p.id == pDataContext.id
                       select p;

            producto pDelabase = prod.FirstOrDefault();
            if (pDelabase != null)
            {
                db.productoes.Remove(pDelabase);
                db.SaveChanges();
                MessageBox.Show("Producto: " + pDataContext.nombre + " Eliminado!!!!");
                this.DataContext = new producto();
            }else
            {
                MessageBox.Show("Producto con Id: " + pDataContext.id + " no existe!!!!");
            }
        }

        private void gridProductos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if(this.gridProductos.SelectedIndex >= 0 && this.gridProductos.SelectedItems.Count >= 0 && this.gridProductos.SelectedItems[0].GetType() == typeof(producto))
            {
                producto pSeleccionado = this.gridProductos.SelectedItems.Cast<producto>().First();
                this.DataContext = pSeleccionado;
            }

        }

        private void limpiar(object sender, RoutedEventArgs e)
        {
            this.DataContext = new producto();
        }

    }
}
