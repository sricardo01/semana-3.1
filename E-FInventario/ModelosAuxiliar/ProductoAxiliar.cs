﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_FInventario.ModelosAuxiliar
{
    class ProductoAxiliar
    {
        public int ID { get; set; }
        public string CODIGO { get; set; }

        public string NOMBRE { get; set; }

        public decimal PRECIO { get; set; }
    }
}
